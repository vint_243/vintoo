#!/usr/bin/env bash

new_root="/mnt/vintoo"
rescue_url_link="http://files.raz-net.com/Distrib/Vintoo/Live"

 echo

 echo "Install Vintoo "

f_disk_l=`lsblk | grep disk | awk ' {print $1} ' `

echo

echo "Enter system disk "
echo

for f_d in $f_disk_l ; do parted /dev/$f_d print ; done

echo
read sys_disk

echo 
echo "System disk /dev/$sys_disk"

echo
echo "Create label and parts"
echo

parted /dev/$sys_disk mklabel msdos mkpart primary ext2 0% 100%

d_label=`parted /dev/$sys_disk print `

echo
echo "$d_label"

echo 
echo "Create Volume Group"
echo

pvcreate /dev/"$sys_disk"1 && vgcreate os /dev/"$sys_disk"1

echo 
echo "Create system label"
echo 
echo "Create one / label [not recomend]  (y/n)"

echo
read root_label

if [[ $root_label == "y" ]] ; then

        echo
        echo "Enter root label size (Gb)"
        echo
	vgs

        echo
	read root_label_sz
	
	echo
        lvcreate -n res_root -L "$root_label_sz"G os
        echo
	vgs

        echo
        echo "Enter swap label size (Gb)"
	
	echo
        read swap_label_sz

        echo
        lvcreate -n swap -L "$swap_label_sz"G os

        echo 
        echo "Create filesystem from (root,swap) parted"

        mkfs.ext4 /dev/os/res_root
        mkswap /dev/os/swap

        echo
        echo "Create dir from system"

        mkdir -pv $new_root
	mount /dev/os/res_root $new_root
        
        root_fstab="1"

fi

if [[ $root_label == "n" ]] ; then


        echo
        echo "Then create label (boot,root,swap,tmp,var) "

	echo
        echo "Enter boot label size (Gb)"
        echo
	vgs
	
	echo
        read boot_label_sz
	
	echo
        lvcreate -n res_boot -L "$boot_label_sz"G os
	
	echo
        echo "Enter root label size (Gb)"
        echo
	vgs
	
	echo
        read root_label_sz
	
	echo
        lvcreate -n res_root -L "$root_label_sz"G os
        echo
	vgs

        echo
        echo "Enter swap label size (Gb)"
	
	echo
        read swap_label_sz
            
	echo
        lvcreate -n swap -L "$swap_label_sz"G os
        echo
	vgs

        echo
        echo "Enter tmp label size (Gb)"
	
	echo
        read tmp_label_sz

	echo
        lvcreate -n res_tmp -L "$tmp_label_sz"G os
        echo
	vgs

        echo
        echo "Enter var label size (Gb)"
	
	echo
        read var_label_sz
	
	echo
        lvcreate -n res_var -L "$swap_label_sz"G os

        echo 
        echo "Create filesystem from (boot,root,tmp,var,swap) parted"
        mkfs.ext4 /dev/os/res_boot
        mkfs.ext4 /dev/os/res_root
        mkfs.ext4 /dev/os/res_tmp
        mkfs.ext4 /dev/os/res_var
        mkswap /dev/os/swap

        echo
        echo "Create dir from system"

	mkdir -pv $new_root
        mount /dev/os/res_root $new_root
        mkdir -pv $new_root/{boot,tmp,var}
        mount /dev/os/res_boot $new_root/boot/
        mount /dev/os/res_tmp $new_root/tmp/
        mount /dev/os/res_var $new_root/var

        root_fstab=2

fi
	src_install="$new_root/tmp/src"
        mkdir -p $src_install

	echo
	echo "Set method installer: "
	echo
	echo "1. Local"
	echo "2. Network"
	echo
	
	read met_install

	
if [[ $met_install == "1" ]] ; then


 	echo 
	echo "Copying precompile root label"
	
	echo
	cp -v /mnt/cdrom/image.squashfs $src_install/

	mkdir -p $src_install/unpack

	mount $src_install/image.squashfs $src_install/unpack -o ro,loop
	
	echo
	echo "Copying install system "

	echo
	cp -avR $src_install/unpack/* $new_root/

	
fi

if [[ $met_install == "2" ]] ; then


        echo 
        echo "Get link Rescue precompile root label "

	echo
        wget $rescue_url_link/current -P $src_install

	link_path=`cat $src_install/current`

        echo
        echo "Download current precompile root label"

        echo
        wget -c $link_path{,.md5} -P $src_install

        name_ark=`cat $src_install/current`
        name_pack=`basename $name_ark`

        echo
        echo "Verify checksum precompile root label"

        md5_sum=`cat $src_install/$name_pack.md5 | awk ' {print $1} '`
        echo "$md5_sum  $src_install/$name_pack" > $src_install/$name_pack.md5

        md5_check=`md5sum -c $src_install/$name_pack.md5 | awk ' {print $2} '`
        md5_res_en="ЦЕЛ"
        md5_res_ru="OK"

        if [[ "$md5_check" == "$md5_res_ru" ]] ; then

                echo
                echo "Checksum md5 verify "
                echo
                echo "Result OK"

            else
            if [[ "$md5_check" == "$md5_res_en" ]] ; then

		                       echo
                        echo "Checksum md5 verify "
                        echo
                        echo "Result OK"
                   else

                echo
                echo "Checksum md5 verify "
                echo
                echo "Result fail"
                echo
                echo "Ignore this fail (y/n)" ; read md5_fail

                echo 

                if [[ $md5_fail == "y" ]] ; then

                        echo 
                        echo "Ignoring this fail"
                        echo
                        echo "Next step"

                     else
                        echo
                        echo "Abort install"
                        exit 0
                        echo "Prodolshenie"
                fi
             fi
        fi

	mkdir -p $src_install/unpack

        mount $src_install/image.squashfs $src_install/unpack -o ro,loop

        echo
        echo "Copying install system "

        echo
        cp -avR $src_install/unpack/* $new_root/
        
fi

	echo
        echo "Umount precompile root label"

        umount $src_install/unpack

        echo
        rm -v $src_install/
	
	echo
        echo "Edit fstab from unpack OS"

        if [[ $root_fstab == "1" ]] ; then

                echo 
                echo "You create one root parted"
		
		fstab="/mnt/gentoo/etc/fstab"
                
		echo "##############################################################" > $fstab
		echo "##############################################################" >> $fstab
		echo "######                                                  ######" >> $fstab  
		echo "######                                                  ######" >> $fstab  
		echo "######                  SYSTEM                          ######" >> $fstab                                          
		echo "######                                                  ######" >> $fstab
		echo "								    " >> $fstab
		echo "								    " >> $fstab
		echo "/dev/mapper/os-res_root /		auto            rw,relatime,user_xattr          0       1" >> $fstab
		echo "/dev/mapper/os-swap    	none            swap            sw                              0       0" >> $fstab
		echo "								    " >> $fstab
		echo "##############################################################" >> $fstab
		echo "##############################################################" >> $fstab
		echo "								    " >> $fstab
		echo "devpts                  /dev/pts        devpts          rw,nosuid,noexec,relatime,gid=5,mode=620       0        0" >> $fstab
		echo "udev                    /dev            devtmpfs        rw,nosuid,relatime,size=10240k,nr_inodes=1013730,mode=755       0       0" >> $fstab
		echo "proc                    /proc           proc            rw,nosuid,nodev,noexec,relatime         0       0" >> $fstab
		echo "							   	   " >> $fstab
		echo "								   " >> $fstab
		echo "#       NFS						   " >> $fstab
		echo "#								   " >> $fstab
		echo "#data_0.raz.net:/mnt/repo       /mnt/repo       nfs4     sync,hard,intr,rw,nolock,rsize=8192,wsize=1024         0 0" >> $fstab
		echo "								   " >> $fstab

fi


	if [[ $root_fstab == "2" ]] ; then

                echo
                echo "You create more parted"

                fstab="/mnt/gentoo/etc/fstab"
                        
                echo "##############################################################" > $fstab 
                echo "##############################################################" >> $fstab
                echo "######                                                  ######" >> $fstab  
                echo "######                                                  ######" >> $fstab  
                echo "######                  SYSTEM                          ######" >> $fstab                                          
                echo "######                                                  ######" >> $fstab
		echo "								    " >> $fstab
		echo "								    " >> $fstab
                echo "/dev/mapper/os-res_root	/		auto            rw,relatime,user_xattr          0       1" >> $fstab
                echo "/dev/mapper/os-res_boot /boot 		ext4 		defaults 			0 	2" >> $fstab
		echo "/dev/mapper/os-res_tmp	/tmp 		ext4 		defaults 			0 	2" >> $fstab
                echo "/dev/mapper/os-res_var 	/var 		ext4 		defaults 			0 	2" >> $fstab	
		echo "								    " >> $fstab
		echo "/dev/mapper/os-swap    	none            swap            sw                              0       0" >> $fstab
		echo "								    " >> $fstab
                echo "##############################################################" >> $fstab
                echo "##############################################################" >> $fstab
		echo "								    " >> $fstab
                echo "devpts                  /dev/pts        devpts          rw,nosuid,noexec,relatime,gid=5,mode=620       0        0" >> $fstab
                echo "udev                    /dev            devtmpfs        rw,nosuid,relatime,size=10240k,nr_inodes=1013730,mode=755       0       0" >> $fstab
                echo "proc                    /proc           proc            rw,nosuid,nodev,noexec,relatime         0       0" >> $fstab
		echo "								    " >> $fstab
		echo "								    " >> $fstab
                echo "#       NFS     						    " >> $fstab
                echo "#								    " >> $fstab
                echo "#data_0.raz.net:/mnt/repo       /mnt/repo       nfs4     sync,hard,intr,rw,nolock,rsize=8192,wsize=1024         0 0" >> $fstab

fi

        echo
        echo "Fstab from unpack OS edited"


echo
echo "Mounting sevice filesystem"

for sv_fs in /dev/ /proc/ /run/ /sys/ ; do

mount --bind $sv_fs /mnt/gentoo$sv_fs

done

echo
echo "Generation grub config, and write grub loader"

grub_i="/mnt/gentoo/tmp/grub.sh"
        
	echo "#!/bin/bash			  " > $grub_i
        echo "		 			  " >> $grub_i
	echo "grub-mkconfig -o /boot/grub/grub.cfg" >> $grub_i
	echo "					  " >> $grub_i
	echo "grub-install /dev/$sys_disk	  " >> $grub_i
	echo "					  " >> $grub_i

chmod +x /mnt/gentoo/tmp/grub.sh

chroot /mnt/gentoo/ /tmp/grub.sh

echo
echo "Install Vintoo Complate"	

echo
echo "Please reboot pc"
echo

